# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.220');


# ---------------------------------------------------------------------- #
# Modify Table deferred_postcode                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE deferred_postcode ADD COLUMN DeferredTimeSlotID INT(11) NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.221');
