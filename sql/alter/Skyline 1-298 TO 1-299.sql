SET foreign_key_checks = 0;


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.298');

# ---------------------------------------------------------------------- #
# Thirumalesh Changes 													 #
# ---------------------------------------------------------------------- # 


ALTER TABLE part_fault_code CHANGE Manufacturer Manufacturer INT( 2 ) NOT NULL ;

ALTER TABLE part_fault_code ADD FieldNumber INT( 2 ) NOT NULL AFTER Status ;

ALTER TABLE part_fault_code CHANGE FieldType FieldType ENUM( 'Generic String', 'Date', 'Number Only' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Generic String';

ALTER TABLE part_fault_code_lookup ADD ModifiedUserID INT( 11 ) NOT NULL , ADD ModifiedDate TIMESTAMP NOT NULL , ADD CreatedDate TIMESTAMP NOT NULL , ADD CreatedUserID INT( 11 ) NOT NULL ;

CREATE TABLE IF NOT EXISTS part_fault_code_lookup_model ( PartFaultCodeLookupModelID int(10) NOT NULL AUTO_INCREMENT, PartFaultCodeLookupID int(10) DEFAULT NULL, ModelID int(10) DEFAULT NULL, ModifiedUserID int(11) NOT NULL, ModifiedDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', CreatedUserID int(11) NOT NULL, PRIMARY KEY (PartFaultCodeLookupModelID) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


# ---------------------------------------------------------------------- #
# Vempati Changes 														 #
# ---------------------------------------------------------------------- # 

INSERT INTO `group_headings` (
`GroupID` ,
`GroupHeading` ,
`Tooltip` ,
`Description` ,
`CreatedDate` ,
`EndDate` ,
`Status` ,
`ModifiedUserID` ,
`ModifiedDate` ,
`Code` ,
`ParentID` ,
`URLSegment`
)
VALUES (
NULL , 'Satisfaction Questionnaire', '', 'This menu item contains a list of satisfaction questions presented to a consumer via the satisfaction questionnaire', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL , '2013-06-28 11:18:00', 'satisfactionQuestionnaire', '2', '/LookupTables/satisfactionQuestionnaire'
);

ALTER TABLE  `questionnaire` ADD  `QuestionnaireBrandID` INT( 11 ) NOT NULL;

ALTER TABLE `questionnaire_log` CHANGE `Brand` `BrandID` INT( 11 ) NOT NULL;


CREATE TABLE IF NOT EXISTS `questionnaire_brand` (
  `QuestionnaireBrandID` int(11) NOT NULL AUTO_INCREMENT,
  `BrandID` int(11) NOT NULL,
  `Status` enum('Active','In-active') NOT NULL,
  `CreatedDate` date NOT NULL,
  `TerminationDate` date DEFAULT NULL,
  PRIMARY KEY (`QuestionnaireBrandID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `questionnaire_brand_attribute` (
  `QuestionnaireBrandAttributeID` int(11) NOT NULL AUTO_INCREMENT,
  `QuestionnaireBrandID` int(11) NOT NULL,
  `QuestionnaireAttributeID` int(11) NOT NULL,
  `AttributeValue` text NOT NULL,
  PRIMARY KEY (`QuestionnaireBrandAttributeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


UPDATE `email` SET `Message` = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>[PAGE_TITLE]</title>
</head>

<body>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family:''Calibri'';">
	<tbody><tr>
		<td bgcolor="#ffffff" align="center">
        	<table width="640" cellspacing="0" cellpadding="0" border="0" style="margin:0 10px;font-family:''Calibri''">
            	<tbody>
                
                <!-- #HEADER
        		================================================== -->
            	<tr>
                	<td width="640"><!-- End top bar table -->
                    </td>
                </tr>
                <tr>
                <td width="640" bgcolor="#52aae0" align="center">
    
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td style="color:#ffffff; font-size:24px; padding-left:10px; text-transform:capitalize;" width="98" height="60px;" valign="bottom">YOUR</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;font-family:''Calibri''" height="60px" colspan="2" valign="bottom">CUSTOMER RECEIPT</td>
        <td style="background:#3576bc; color:#ffffff;color:#ffffff; font-size:24px; padding-left:20px;padding-bottom:2px;padding-right:5px; font-family:''Calibri''" height="30" colspan="2" valign="bottom"><p align="right" style="font-size:13px; color:#ffffff; margin:2px;font-family:''Calibri''">Email us: <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]" >[BRANCH_EMAIL]</a></p></td>
        </tr>
        
    </tbody></table>
    
    <!-- End header -->
</td>
                </tr>
                <!-- #CONTENT
        		================================================== -->
                <tr><td width="640" height="10" bgcolor="#ffffff"></td></tr>
                <tr>
                <td width="640" bgcolor="#ffffff">
    <table width="640" cellspacing="6" cellpadding="6" border="0">
        <tbody><tr>
            <td width="30"></td>
            <td width="580">
                <!-- #TEXT ONLY
                ================================================== -->
                <table width="580" cellspacing="6" cellpadding="6" border="0">
                    <tbody><tr>
                        <td width="405">
                            <p style="font-size:13px;font-family:''Calibri'';color:#444444;">Dear [CUSTOMER_TITLE_SURNAME]</p>
                            
							
				                <p style="font-size:15px; color:#444444;line-height:115%;font-family:''Calibri'';">Following the recent service we supplied we sincerely hope that we satisfied your requirement and that the standard of service exceeded your expectations.  Should you experience any issues with this product or any other similar products, please feel free to contact us to arrange a new service request.</p>
                                <p style="font-size:15px; color:#444444;line-height:115%;font-family:''Calibri''"></p>
                                

                        </td>
                        <td width="175">
						                        
                          <img width="[BRAND_LOGO_WIDTH]" height="[BRAND_LOGO_HEIGHT]" alt="[BRAND_NAME]"  title="[BRAND_NAME]" src="[BRAND_LOGO]" />
                       
					    </td>
                    </tr>
                   
                </tbody></table>
                <table>
                <tbody>
                
                    <tr><td width="580" height="2" style="background:#3576bc;font-family:''Calibri''"></td></tr>
                   
                    </tbody>
                    </table>
                                        
                <!-- #TEXT WITH ADDRESS
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="2" border="0">
                    <tbody>
					
                    <tr>
                        <td colspan="2" style="padding-bottom:25px;text-align:center;"  ><br>
                          <a href="[SERVICE_QUESTIONNAIRE]" style="color:#3576bc;font-size:16px;font-weight:bold;" >Please take a few minutes to complete our Service Questionnaire</a>
                       </td>
                        
                    </tr>
					
					
			    </tbody></table>
                                        
           
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                    <tr><td width="580" height="3" style="background:#3576bc;font-family:''Calibri''"></td></tr>
                </tbody></table>
                                        
                <!-- #TEXT FOOTER
                ================================================== -->
                <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        
                    </tr>
                    <tr><td width="580" height="100">
                    

<p style="font-size:13px;color:#444444;margin-bottom:0px;line-height:115%;font-family:''Calibri''"></p>
<p style="font-size:13px;color:#444444;margin-top:0px;line-height:115%;font-family:''Calibri''">Assuring you of our best intention at all times.</p>

<p style="font-size:13px;color:#444444;line-height:115%;font-family:''Calibri''">Yours Sincerely, <br />[BRAND_NAME] Customer Service Team</p>
                    </td>
                    </tr>
                </tbody></table>
                                                                                
                <!-- #SIDE-BY-SIDE ARTICLE SECTIONS WITH TEXT ONLY
                ================================================== -->
               <!-- <table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td valign="top">
                          
                        </td>
                       
                    </tr>
                </tbody></table>-->
                                        
                <!-- #ADDITIONAL IMAGE GALLERY ROW WITH NO TITLE
                ================================================== -->
                <!--<table width="580" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                        <td width="180" valign="top">
                          
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                       
                        </td>
                        <td width="20"></td>
                        <td width="180" valign="top">
                            
                        </td>
                    </tr>
                </tbody></table>-->
            </td>
            <td width="30"></td>
        </tr>
    </tbody></table>
</td>
                </tr>
                <!--<tr><td width="640" height="15" bgcolor="#ffffff"></td></tr>-->
                <!-- #FOOTER
   				 ================================================== -->
                <tr>
                <td width="640">
    <table width="640" cellspacing="0" cellpadding="0" border="0">
        <tbody><tr>
        <td width="640" colspan="3" rowspan="3" height="20px" style="background:#3576bc;font-family:''Calibri''">
 <p style="font-size:13px;color:#ffffff; text-align:center;font-family:''Calibri''">If you have any queries contact us on <a style="color:#ffffff;font-family:''Calibri''" href="mailto:[BRANCH_EMAIL]">[BRANCH_EMAIL]</a></p>
            </td>
            </tr>

    </tbody></table>
</td>
                </tr>
               
            </tbody></table>
        </td>
	</tr>
</tbody></table>
</body>
</html>' WHERE `email`.`EmailID` = 16;

DROP TRIGGER IF EXISTS after_questionnaire_log_insert;

DELIMITER ;;

CREATE TRIGGER `after_questionnaire_log_insert` AFTER INSERT ON `questionnaire_log` FOR EACH ROW 
BEGIN
        DECLARE Qbrandid integer;
        /*Check Questionnaire form for the brand is exiting , if not exiting insert defult Attribute Data*/
        IF(NOT EXISTS (SELECT BrandID FROM questionnaire_brand WHERE BrandID = NEW.BrandID) ) THEN 
                INSERT INTO questionnaire_brand
                             SET    BrandID = NEW.BrandID,
                                    STATUS = 'Active',
                                    CreatedDate = CURDATE( ) ;

                SELECT LAST_INSERT_ID() INTO Qbrandid;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '4',
                                AttributeValue = 'Ease of booking' ;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '5',
                                AttributeValue = 'Speed of service (to first visit)' ;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '6',
                                AttributeValue = 'Speed of service (overall)' ;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '7',
                                AttributeValue = 'Communication throughout' ;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '8',
                                AttributeValue = 'Engineer (where relevant)' ;

                INSERT INTO questionnaire_brand_attribute
                            SET QuestionnaireBrandID = Qbrandid,
                                QuestionnaireAttributeID = '10',
                                AttributeValue = 'Retailer|Insurer/Extended Warrantor|Advertisement|Manufacturer|Web Search|Personal Recommendation' ;

        END IF;

END
;;

DELIMITER ;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.299');

SET foreign_key_checks = 1;

