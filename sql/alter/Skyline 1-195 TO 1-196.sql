# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.195');

# ---------------------------------------------------------------------- #
# Add table "report_progress"                                            #
# ---------------------------------------------------------------------- #
CREATE TABLE `report_progress` (
									`Hash` VARCHAR(40) NULL, 
									`Status` ENUM('running','finished') NULL, 
									`TotalRows` INT(11) NULL, `CurrentRow` INT(11) NULL 
								) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.196');
