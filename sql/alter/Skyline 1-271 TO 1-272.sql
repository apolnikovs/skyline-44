# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.271');

# ---------------------------------------------------------------------- #
# Modify table ra_status                                                 #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_status ADD COLUMN SubsetOnly ENUM('Yes','No') NULL DEFAULT NULL AFTER SBAuthorisationStatusID;

# ---------------------------------------------------------------------- #
# Andris Stock System Changes                                            #
# ---------------------------------------------------------------------- # 

ALTER TABLE sp_part_stock_template CHANGE COLUMN PurchaseCost PurchaseCost DECIMAL(10,2) NULL DEFAULT NULL AFTER PartNumber;

CREATE TABLE job_fault_code_lookup ( JobFaultCodeLookupID INT(10) NOT NULL AUTO_INCREMENT, LookupName VARCHAR(10) NULL DEFAULT NULL, Description VARCHAR(50) NULL DEFAULT NULL, ServiceProviderManufacturerID INT(11) NULL DEFAULT NULL, Status ENUM('Active','Inactive') NULL DEFAULT NULL, ExcludeFromBouncerTable ENUM('Yes','No') NULL DEFAULT NULL, ForcePartFaultCode ENUM('Yes','No') NULL DEFAULT NULL, PartFaultCodeNo INT(11) NULL DEFAULT NULL, JobTypeAvailability ENUM('Chargeable Jobs','Warranty Jobs','Both') NULL DEFAULT NULL, PromptForExchange ENUM('Yes','No') NULL DEFAULT NULL, RelatedPartFaultCode INT(11) NULL DEFAULT NULL, RepairTypeIDForChargeable INT(11) NULL DEFAULT NULL, RepairTypeIDForWarranty INT(11) NULL DEFAULT NULL, RestrictLookup ENUM('Yes','No') NULL DEFAULT NULL, RestrictLookupTo ENUM('Part Used','Part Correction','Adjustment') NULL DEFAULT NULL, ServiceProviderID INT(11) NULL DEFAULT NULL, CreatedUserID INT(11) NULL DEFAULT NULL, CreatedDate TIMESTAMP NULL DEFAULT NULL, ModifiedDate TIMESTAMP NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (JobFaultCodeLookupID), INDEX ServiceProviderManufacturerID (ServiceProviderManufacturerID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE job_fault_code ( JobFaultCodeID INT(11) NOT NULL AUTO_INCREMENT, ServiceProviderID INT(11) NOT NULL, RepairTypeID INT(11) NULL DEFAULT NULL, JobFaultCode VARCHAR(50) NULL DEFAULT NULL, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', FieldNo INT(11) NULL DEFAULT NULL, FieldType ENUM('Generic String','Date','Number Only') NOT NULL, Lookup ENUM('Yes','No') NOT NULL DEFAULT 'No', NonFaultCode ENUM('Yes','No') NOT NULL DEFAULT 'No', MainInFault ENUM('Yes','No') NOT NULL DEFAULT 'No', MainOutFault ENUM('Yes','No') NOT NULL DEFAULT 'No', UseAsGenericDescription ENUM('Yes','No') NOT NULL DEFAULT 'No', RestrictLength ENUM('Yes','No') NOT NULL DEFAULT 'No', LengthFrom INT(11) NULL DEFAULT NULL, LengthTo INT(11) NULL DEFAULT NULL, ForceFormat ENUM('Yes','No') NOT NULL DEFAULT 'No', Format VARCHAR(15) NULL DEFAULT NULL, ReplicateToFaultDescription ENUM('Yes','No') NOT NULL DEFAULT 'No', FillFromDOP ENUM('Yes','No') NOT NULL DEFAULT 'No', HideWhenRelatedFaultCodeBlank ENUM('Yes','No') NOT NULL DEFAULT 'No', FaultCodeNo INT(11) NULL DEFAULT NULL, HideForThirdPartyJobs ENUM('Yes','No') NOT NULL DEFAULT 'No', NotReqForThirdPartyJobs ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqAtBookingForChargeable ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqAtCompletionForChargeable ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqAtBookingForWarranty ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqAtCompletionForWarranty ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqIfExchangeIssued ENUM('Yes','No') NOT NULL DEFAULT 'No', ReqOnlyForRepairType ENUM('Yes','No') NOT NULL DEFAULT 'No', UseLookup ENUM('Yes','No') NOT NULL DEFAULT 'No', ForceLookup ENUM('Yes','No') NOT NULL DEFAULT 'No', ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, CreatedDate TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00', CreatedUserID INT(11) NULL DEFAULT NULL, PRIMARY KEY (JobFaultCodeID), INDEX ServiceProviderID (ServiceProviderID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE job_fault_code_lookup_to_job_fault_code ( JobFaultCodeLookupToJobFaultCode INT(10) NOT NULL AUTO_INCREMENT, JobFaultCodeLookupID INT(10) NOT NULL DEFAULT '0', JobFaultCodeID INT(10) NOT NULL DEFAULT '0', ModifiedUserID INT(10) NOT NULL DEFAULT '0', ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (JobFaultCodeLookupToJobFaultCode) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB AUTO_INCREMENT=1;

CREATE TABLE job_fault_code_lookup_restricted_models ( JobFaultCodeLookupRestrictedModels INT(10) NOT NULL AUTO_INCREMENT, JobFaultCodeLookupID INT(10) NOT NULL, ServiceProviderModelID INT(10) NOT NULL, ModifiedUserID INT(10) NOT NULL, ModifiedDate TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (JobFaultCodeLookupRestrictedModels), CONSTRAINT FK__job_fault_code_lookup FOREIGN KEY (JobFaultCodeLookupID) REFERENCES job_fault_code_lookup (JobFaultCodeLookupID) ON UPDATE RESTRICT ON DELETE RESTRICT, CONSTRAINT FK__service_provider_model FOREIGN KEY (ServiceProviderModelID) REFERENCES service_provider_model (ServiceProviderModelID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.272');
