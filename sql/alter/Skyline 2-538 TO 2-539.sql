# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- #
call UpgradeSchemaVersion('2.538');

# ---------------------------------------------------------------------- #
#  ISSUE 2364 - Andris Polnikovs								 		 #	
# ---------------------------------------------------------------------- #
ALTER TABLE diary_route_history_data ADD COLUMN AppointmentDate DATE NULL DEFAULT NULL AFTER SortOrder;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
INSERT into version (VersionNo) VALUES ('2.539');	