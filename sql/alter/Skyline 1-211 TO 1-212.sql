# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.211');

# ---------------------------------------------------------------------- #
# Modify Table manufacturer	                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE manufacturer ADD Acronym VARCHAR( 30 ) NULL DEFAULT NULL AFTER ManufacturerName; 


# ---------------------------------------------------------------------- #
# Modify Table brand	                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE brand ADD Acronym VARCHAR( 30 ) NULL DEFAULT NULL AFTER BrandName;
				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.212');
