<?php
/**
 * UtlAPIContoller.class.php
 * 
 * Implementation of Utl API for SkyLine (server
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 10/05/2013  1.00    Andrew J. Williams    Initial Version
 ********************************************************************************/

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');
include_once(APPLICATION_PATH.'/include/xml2array.php');

class UtlAPI extends SkylineAPI {
    public $debug = true;

    
    /**
     * Description
     * 
     * Deal with post  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function post( $args ) {
        
        if($this->debug) $this->log("UtlAPIController::post  ".  var_export(@file_get_contents('php://input'),true),'utl_');
       
                $params = xml2array(@file_get_contents('php://input'));         /* Get XML payload body */    


                $response = $this->utlRepairStatusAction($params);
                
                $httpcode=200;

        $this->sendResponse($httpcode, $response);    
    }
    
    /**
     * utlRepairStatusAction
     * 
     * Deal with job updates containing repair details send from UTL
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function utlRepairStatusAction( $params ) {
        
        $job_model = $this->loadModel('Job');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $part_model = $this->loadModel('Part');
        $skyline_model = $this->loadModel('Skyline');
        $status_history_model = $this->loadModel('StatusHistory');
        $users_model = $this->loadModel('Users');
        
        $job_fields = array();                                                  /* Array to hold fields for the job table */
        
        if (! is_array($params) ) {                                             /* Check we have a valid array out of the xml */
            $this->log('utlRepairStatusAction : Invalid XML','utl_');
            $this->log($params);
            return(false);
        }
        
        if (! isset($params['REPAIR_STATUS']) ) {                               /* Check if we have the repair status container in the xml */
            $this->log('utlRepairStatusAction : No repair Status in XML','utl_');
            $this->log($params);
        } else {
            /* Deal with repair status */
            $info = $params['REPAIR_STATUS']['REPAIR_STATUS_INFO'];

            /*
            *  Build up fields for jobs table
            */

            $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo($info['ASC_CODE']);
            if (is_null($network_service_provider_array)) {
                $msg = array(
                            'Code' => 'ERROR',
                            'Response' => "getServiceRequest : Account number (ASC_CODE) {$params['ASREQUEST_INFO']['ASC_CODE']} not found."
                            );
                $this->log($msg['Response'],'utl_api_');
                return($msg);
            }
            $job_fields['ServiceProviderID'] = $network_service_provider_array['ServiceProviderID'];
            $job_fields['ModifiedDate'] = date('Y-m-d H:i:s');
            $job_fields['NetworkID'] = $network_model->getNetworkId($this->config['Utl']['NetworkName']);
            $job_fields['ClientID'] = $skyline_model->getNetworkDefaultClient($job_fields['NetworkID']);
            if (is_null($job_fields['ClientID'])){
                $msg = array(
                            'Code' => "ERROR",
                            'Response' => "getServiceRequest : Cannot find client for ClientAccountNo (ASC_CODE) {$params['ASREQUEST_INFO']['ASC_CODE']} from Network {$job_fields['NetworkID']}"
                            );
                $this->log($msg['Response'],'utl_api_');                         
                return($msg);
            } 
            $u_id = $users_model->getNetworkUserID($job_fields['NetworkID']);
            $job_fields['ModifiedUserID'] = $u_id ;
            if (isset ($info['MODEL_CODE']) ) {
                $job_fields['ServiceBaseModel'] = substr((string) $info['MODEL_CODE'],0,40);
                $job_fields['ModelID'] = $models_model->getModelIdFromNo($info['MODEL_CODE']);           /* Get the ID of the model */   
                if ( is_null($job_fields['ModelID']) ) {                            /* Do we have the model ID */
                    unset($job_fields['ModelID']);                                  /* Not found, so don't set it */
                } else {
                    unset($job_fields['ServiceBaseModel']);                         /* Have it, don't set ServiceBaseModek */
                }
            }
            if (isset ($info['REPAIR_ETD_DATE']) ) {
                $job_fields['ETDDate'] = date( 'Y-m-d', strtotime ($info['REPAIR_ETD_DATE']));
                $job_fields['ETDTime'] = date( 'H:i:s', strtotime ($info['REPAIR_ETD_TIME']));
            }
            $job_fields['Notes'] = '';
            if ( isset($info['STATUS_COMMENT']) ) $job_fields['Notes'] .=  date( 'Y-m-d H:i')." {$info['STATUS_COMMENT']}";
            if ( isset($info['WARRANTY_LABOR']) ) $job_fields['SamsungWarrantyLabour'] = substr((string) strtoupper($info['WARRANTY_LABOR']),0,1);
            if ( isset($info['WARRANTY_PARTS']) ) $job_fields['SamsungWarrantyParts'] = substr((string) strtoupper($info['WARRANTY_PARTS']),0,1);
            if ( (isset($job_fields['SamsungWarrantyLabour'])) && (isset($job_fields['SamsungWarrantyParts'])) ) {
                if ( ($job_fields['SamsungWarrantyLabour'] == 'I') && ($job_fields['SamsungWarrantyParts'] == 'O') ) {                           /* Check if part warranty */
                    $job_fields['Notes'] .= "\nLABOUR ONLY CLAIM";
                } elseif ( ($job_fields['SamsungWarrantyLabour'] == 'O') && ($job_fields['SamsungWarrantyParts'] == 'I') ) {
                    $job_fields['Notes'] .= "\nPARTS ONLY CLAIM";
                }
            } /* fi isset */
            if ( isset($info['PRODUCT_RECEIVED_DATE']) ) $job_fields['DateUnitReceived'] = date( 'Y-m-d', strtotime($info['PRODUCT_RECEIVED_DATE']));
            if ( isset($info['ENGINEER_CODE']) ) $job_fields['EnineerCode'] = $info['ENGINEER_CODE'];
            if ( isset($info['ENGINEER_NAME']) ) $job_fields['EngineerName'] = $info['ENGINEER_NAME'];
            if ( isset($info['ENG_ASSIGN_DATE']) ) $job_fields['EngineerAssignedDate'] = date( 'Y-m-d', strtotime ($info['ENG_ASSIGN_DATE']));
            if ( isset($info['ENG_ASSIGN_TIME']) ) $job_fields['EngineerAssignedTime'] = date( 'H:i:s', strtotime($info['ENG_ASSIGN_TIME']));
            
            $jId = $info['TR_NO'];

            $trstatus = $info['TR_STATUS'];                                         /* Get Reason and Status to calcilate Skylien Reason and Status */
            $trreason = $info['TR_REASON'];
            $status = array();

            if ( ($trstatus == 'ST015') && ($trreason == 'HA005') ) {
                $status[] = '05 FIELD CALL REQUIRED';
            }

            if ( ($trstatus == 'ST015') && ($trreason == 'HAZ04') ) {
                $status[] = '06 FIELD CALL ARRANGED';
            }

            if ( ($trstatus == 'ST025') && ($trreason == 'HAZ041') ) {
                $status[] = '07 UNIT IN REPAIR';
            }

            if ( ($trstatus == 'ST030') && ($trreason == 'HP030') ) {
                $status[] = '08 ESTIMATE SENT';
            }

            if ( ($trstatus == 'ST025') && ($trreason == 'HEZ01') ) {
                $status[] = '09 ESTIMATE ACCEPTED';
            }

            if ( ($trstatus == 'ST052') && ($trreason == 'HCZ04') ) {
                $status[] = '10 ESTIMATE REFUSED';
                $job_fields['CompletionStatus'] = 'BER';
                $job_fields['ClosedDate'] = date('Y-m-d');
            }

            if ( ($trstatus == 'ST030') && ($trreason == 'HPZ02') ) {
                $status[] = '12 PARTS ORDERED';
            }

            if ( ($trstatus == 'ST030') && ($trreason == 'HPZ03') ) {
                $status[] = '13 PARTS RECEIVED';
            }

            if ( ($trstatus == 'ST030') && ($trreason == 'HP015') ) {
                $status[] = '14 2ND FIELD CALL REQ.';
            }

            if ( ($trstatus == 'ST030') && ($trreason == 'HP035') ) {
                $status[] = '16 AUTHORITY REQUIRED';
            }

            if ( ($trstatus == 'ST025') && ($trreason == 'HEZ01') ) {
                $status[] = '17 AUTHORITY ISSUED';
            }

            if ( ($trstatus == 'ST025') && ($trreason == 'HEZ01') ) {
                $status[] = '18 FIELD CALL COMPLETE';
            }

            if ( 
                    ( ($trstatus == 'ST035') && ($trreason == 'HL015') )
                    || ( ($trstatus == 'ST070') && ($trreason == '') )
                ) {
                $status[] = '19 DELIVER BACK REQ.';
            }

            if (  ($trstatus == 'ST035') && ($trreason == 'HGZ01') ) {
                $status[] = '20 DELIVER BACK ARR.';
            }

            if ( 
                    ( ($trstatus == 'ST035') && ($trreason == 'HL005') )
                    || ( ($trstatus == 'ST040') && ($trreason == 'HGZ01') )
                    || ( ($trstatus == 'ST070') && ($trreason == '') )
                ) {
                $status[] = '18 REPAIR COMPLETED';
                $status[] = '21C INVOICED';
                $status[] = '21W CLAIMED';
                $status[] = '26 INVOICE RECONCILED';
                if ($trreason == 'HL005') {                                     /* Repair Completed */
                    $job_fields['ClosedDate'] = date('Y-m-d');
                }
                if ($trreason == 'HGZ01') {
                    $job_fields['ServiceProviderDespatchDate'] = date('Y-m-d');
                }
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HC015') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['ClosedDate'] = date('Y-m-d');
                $job_fields['CompletionStatus'] = 'WRONG MANUFACTURER';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HC025') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['ClosedDate'] = date('Y-m-d');
                $job_fields['CompletionStatus'] = 'CUSTOMER NOT CONTACTABLE';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HC030') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'REQUEST BY CUSTOMER';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HCZ08') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'EXCHANGED';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HC005') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['ClosedDate'] = date('Y-m-d');
                $job_fields['CompletionStatus'] = 'NO FAULT FOUND';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HCZ01') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'DUPLICATE JOB';
            }

            if (  ($trstatus == 'ST052') && ($trreason = 'HCZ24') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'PARTS ISSUE';
            }        

            if (  ($trstatus == 'ST052') && ($trreason = 'HCZ03') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'RESCHEDULE';
            }   

            if (  ($trstatus == 'ST052') && ($trreason = 'HCZ04') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'COST ISSUE';
            }  

            if (  ($trstatus == 'ST052') && ($trreason = 'HC015') ) {
                $status[] = '29 JOB CANCELLED';
                $job_fields['CompletionStatus'] = 'WRONG ASC';
            } 

            if (  ($trstatus == 'ST030') && ($trreason == 'HP025') ) {
                $status[] = '30 OUT CARD LEFT';
            }

            if (  ($trstatus == 'ST030') && ($trreason == 'HP010') ) {
                $status[] = '31 WAITING FOR INFO';
            }

            if (  ($trstatus == 'ST025') && ($trreason == 'HEZ01') ) {
                $status[] = '32 INFO RECEIVED';
            }

            if (  ($trstatus == 'ST030') && ($trreason == 'HP040') ) {
                $status[] = '33 PART NO LONGER AVAILABLE';
            }

            if (  ($trstatus == 'ST015') && ($trreason == 'HA010') ) {
                $status[] = '34 PACKAGING SENT';
            }

            if (  ($trstatus == 'ST015') && ($trreason == 'HAZ05') ) {
                $status[] = '34 PACKAGING SENT';
            }

            if (  ($trstatus == 'ST015') && ($trreason == 'HAZ06') ) {
                $status[] = '40 CX NOT AVAIL - NO RESPONSE';
            }

            if (  ($trstatus == 'ST015') && ($trreason == 'HAZ07') ) {
                $status[] = '41 CX NOT AVAIL - WRONG NUM';
            }
            
            if ( count($status) > 0 ) {                                         /* Check if we are setting status */
                $job_fields['Status'] = $status[count($status)-1];              /* Get the last status item to write into the status text field on job table */
            } /* fi count($status) > 0*/
            
            $job_response = $job_model->update($job_fields);                    /* Pass these fields to the jobs model and create */

            if ($job_response['jobId'] == 0) {                                  /* Check the JobID for the created job */
                $this->log("utlRepairStatusAction : Record update error. PDO Error : {$job_response['message']}",'utl_api_');
                continue;
            }

            for ($n = 0; $n < count($status); $n++ ) {                          /* Loop through each status */
                $status_fields = array(
                                       'JobID' => $jId,
                                       'StatusID' => $skyline_model->getStatusID($status[$n]),
                                       'AdditionalInformation' => "Updated by UTL - TR_STATUS: $trstatus  TR_REASON: $trreason",
                                       'UserID' => $u_id,
                                       'Date' => date('Y-m-d H:i:s')
                                      );

                $status_response = $status_history_model->create($status_fields);
                if ($this->debug) $this->controller->log($status_response, 'utl_api_');
            } /* next n */
        } /* fi isset REAPIR_STATUS */
        
        /* Deal with repair parts */
        if ( isset($params['REPAIR_STATUS']['REPAIR_PART_INFO']) ) {
            $parts = $params['REPAIR_STATUS']['REPAIR_PART_INFO'];
            if ( is_array($parts) ) {
                foreach ($parts as $part) {
                    $part_fields = array(
                        'JobID' => $part['TR_NO'],
                        'OrderNo' => $part['PART_ORDER_NO'],
                        'PartSerialNo' => $part['PART_NO'],
                        'Quantity' => $part['PART_QTY'],
                        'OrderStatus' => $part['PART_STATUS']
                    );
                    
                    $part_model->create($part_fields);
                } /* next $ part */
            } else {
                $part_fields = array(
                    'JobID' => $parts['TR_NO'],
                    'OrderNo' => $parts['PART_ORDER_NO'],
                    'PartSerialNo' => $parts['PART_NO'],
                    'Quantity' => $parts['PART_QTY'],
                    'OrderStatus' => $parts['PART_STATUS']
                );

                $part_model->create($part_fields);
            } /* fi is array $parts */
        } /* fi isset REPAIR_PART_INFO */
    } 
    
    
}
?>
