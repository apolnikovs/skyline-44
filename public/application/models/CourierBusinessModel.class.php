<?php

require_once('CustomBusinessModel.class.php');
require_once('Functions.class.php');
require_once('Constants.class.php');

/**
 * Short Description of Courier Business Model.
 * 
 * Long description of Courier Business Model.
 *
 * @author     Brian Etherington <b.etherington@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       http://www.pccontrolsystems.com
 * @version    1.2
 * 
 *  
 * Changes
 * Date       Version  Author                Reason
 * 25/03/2013 1.0      Brian Etherington     Initial Version
 * 07/05/2013 1.1      Brian Etherington     Allow collection date to be empty
 * 29/05/2013 1.2      Brian Etherington     Allow County and Country to be empty
 * **************************************************************************** */
class CourierBusinessModel extends CustomBusinessModel {

    public function __construct($controller) {
        parent::__construct($controller);
    }
    
    public function BookCourierCollection( array $args, $UpdateCourierAddress=true ) {
                              
        // Update Job and Book the courier collection
        $job_id = $args['JobID'];
                
        // Update the job table
        $job_model = $this->loadModel('Job');  
        // convert dd/mm/yy to yyyy-mm-dd
        
        $collection_date = empty($args['CollectionDate']) ? null 
                                                          : date('Y-m-d', strtotime(str_replace('/', '-', $args['CollectionDate'])));
        $job_fields = array(
                    'JobID' => $job_id,
                    //'' => $args['Direction'];
                    'CollectionDate' => $collection_date,
                    'CourierID' => empty($args['PreferredCourier']) ? null : $args['PreferredCourier']
                );
        if ($UpdateCourierAddress) {
            $job_fields['ColAddCompanyName'] = $args['Name'];
            $job_fields['ColAddPostcode'] = $args['Postcode'];
            $job_fields['ColAddBuildingNameNumber'] = $args['BuildingName'];
            $job_fields['ColAddStreet'] = $args['Street'];
            $job_fields['ColAddLocalArea'] = $args['Area'];
            $job_fields['ColAddTownCity'] = $args['Town'];
            $job_fields['ColAddCountyID'] = empty($args['County']) ? null : $args['County'];
            $job_fields['ColAddCountryID'] = empty($args['Country']) ? null : $args['Country'];
            $job_fields['ColAddEmail'] = $args['Email'];
            $job_fields['ColAddPhone'] = $args['Phone'];
        }
        $job_result = $job_model->update( $job_fields );
                                              
        // Insert/Update the shipping table
        // Note: these fields are duplicated in the Job table
        //       for reasons I don't understand.
        $shipping_model = $this->loadModel('Shipping');
        //$shipping_model->debug = true;
        $shipping_fields = array(
                    'JobID' => $job_id,
                    'ConsignmentDate' => $collection_date,
                    'CourierID' => empty($args['PreferredCourier']) ? null : $args['PreferredCourier'],
                    'ConsignmentNo' => $args['ConsignmentNo'],
                    'PackagingType' => empty($args['PackagingType']) ? null : $args['PackagingType']
                );
             
        // 26/03/2013 Change requested By Vic/Joe.  
        // Do not update the courier record but always insert a new one.
        /*$sql = 'select ShippingID from shipping where JobID=:JobID';
        $query_params = array ( 'JobID' => $job_id );
        $query_result = $shipping_model->Select( $sql, $query_params );
                
        if (count($query_result) == 0) {
            $shipping_result = $shipping_model->Add( $shipping_fields );   
        } else {
            $shipping_fields['ShippingID'] = $query_result[0]['ShippingID'];
            $shipping_result = $shipping_model->Update( $shipping_fields );  
        }*/
        
        $shipping_result = $shipping_model->Add( $shipping_fields );
                     
        // return if no courier selected
        if (empty($args['PreferredCourier'])) return;
               
        // Get Courier Details...
        $couriers_model = $this->loadModel('Couriers');
        $sql = 'select * from courier where CourierID=:CourierID';
        $query_params = array( 'CourierID' => $args['PreferredCourier'] );      
        $query_result = $couriers_model->Select($sql, $query_params);
                
        // Check if courier is DPD
        if (count($query_result) > 0
                && $query_result[0]['CourierName'] == 'DPD'
                && $query_result[0]['Online'] == 'Yes'
                && $query_result[0]['CommunicateIndividualConsignments'] == 'Yes') {  
            // call the DPD courier booking FTP process
            $courier_dpd_model = $this->loadModel('CourierDPD');
            $courier_dpd_model->sendSingleSwapitJob( $job_id );
        }
               
    }
    
}

?>
