<?php
/**
 * Skillset.class.php
 * 
 * Routines for interaction with the skillset table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.02
 * 
 * Changes
 * Date        Version Author                Reason
 * 19/10/2012  1.00    Andrew J. Williams    Initial Version
 * 22/10/2012  1.01    Nageswara Rao Kanteti Added getSkillset
 * 31/01/2013  1.02    Andrew J. Williams    Added getIdByUnitTypeAppointmentType
 * 20/02/2013  1.03    Andrew J. Williams    Issue 227 - Samsung One Touch - Skillset ID Not Being returned
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class Skillset extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::Skillset();
    }
    
    /**
     * fetchById
     *  
     * Return the record associated with a specific Skillset ID
     * 
     * @param integer $ssId     ID of the skillset
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function fetchById($ssId) {
        $sql = "
                SELECT 
                        * 
                FROM 
                    `skillset`
                WHERE 
                    SkillsetID=:SkillsetID";
        
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $fetchQuery->execute(array(':JobTypeID' => $ssId));
        $result = $fetchQuery->fetch();
        
        return($result);
    }
    
    /**
     * fetchByName
     *  
     * Return the record associated with a specific Skillset ID
     * 
     * @param string $ssName     Name of the skillset
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function fetchByName($ssName) {
        $sql = "
                SELECT 
                        * 
                FROM 
                    `skillset`
                WHERE 
                    SkillsetName = '$ssName'
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]);                                                 /* Skillset name found so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    
    public function getSkillset($repID,$aptID){
       $sql="select * from skillset where RepairSkillID=$repID and AppointmentTypeID=$aptID";
      
        $res=$this->Query($this->conn, $sql);
       
        if ( count($res) > 0 ) {
            return($res[0]);                                                 /* Entry founs so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
    /**
     * getIdByUnitTypeAppointmentType
     *  
     * Return the ID of a specific skillset based on Appointment Type and Unit
     * Type ID
     * 
     * @param string $atId      Appointment Type ID
     *        integer $utId     Unit Type id
     * 
     * @return      integer with skillset id
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getIdByUnitTypeAppointmentType($atId, $utId) {
        $sql = "
                SELECT 
                        ss.`SkillsetID` 
                FROM
			`skillset` ss,
			`unit_type` ut
		WHERE
			ut.`RepairSkillID` = ss.`RepairSkillID`
			AND ss.`AppointmentTypeID` = $atId
			AND ut.`UnitTypeID` = $utId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['SkillsetID']);                                     /* Entry founs so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
}

?>
