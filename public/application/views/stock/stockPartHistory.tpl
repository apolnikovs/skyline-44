 <script type="text/javascript">   
     
 $(document).ready(function() {
 
  $('#stockHistoryDataTable').dataTable( {
"sDom": 'f<bottom>pi',
//"sDom": 'Rlfrtip',
"bServerSide":true,
"bPaginate":true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 ], [25, 50, 100]],

		 "oLanguage": {
                                
                                "sSearch": "Filter Results By:"
                            },
    "sAjaxSource": "{$_subdomain}/Stock/loadStockHistoryList/id={$id}/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                              
                                $.colorbox.resize();
                               
                               
			} );
                        },
                        
                            
                        

"iDisplayLength" : 25,

"aoColumns": [ 
			 { "bVisible":1, "bSearchable":1},
			 { "bVisible":1, "bSearchable":1},
			 { "bVisible":1, "bSearchable":0},
			 { "bVisible":1, "bSearchable":1},
                              
                            
		],
                "bFilter": true,
                 "aaSorting": [ [0,'desc'] ]
              
               
   
 
        
          
});//datatable end
   }); 
   
  </script>  
   
<div id="StockHistoryPopupContainer" class="SystemAdminFormPanel" >
    <fieldset>
        <legend>Stock Part History</legend>
         <label  style="width:120px;font-size:12px">Stock No:</label><span >{$Data.PartNumber|default:''}</span><br>
         <label  style="width:120px;font-size:12px">Description:</label><span >{$Data.Description|default:''}</span><br>
         <label  style="width:120px;font-size:12px">Current Qty:</label><span >{$Data.InStockAvailable|default:''}</span><br>
    
            <table id="stockHistoryDataTable" border="0" cellpadding="0" cellspacing="0" class="browse">
                <thead>
                    
                <tr>
                  
                    <th>Date</th>
                    <th>Action</th>
                    <th>Qty</th>
                    <th>Ref. No</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
          
            <br>
              <button  style="width:100px;float:right" onclick="$.colorbox.close();" id="ReceivingFinishButton" type="button" class="gplus-blue">Finish</button>
        
    
    </fieldset>    
                 
                        
       
</div>
                 
 
                          
                        
