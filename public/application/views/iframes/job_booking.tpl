{extends "iframes/MasterLayout.tpl"}


{block name=config}
{$Title = 'Skyline Job Booking'}
{/block}
  
{block name=scripts}
<script>
$.tools.validator.fn('[data-group]', 'Please complete at least one of these mandatory fields', function( input, value ) { 
            var group = input.attr('data-group');
            var is_valid = false;
            $('input[data-group='+group+']').each(
                    function() {
                        if (this.value !== '') is_valid = true;
                    }
                );
            return is_valid;
});

$(document).ready(function() {

    $("#bookingform").validator({
            message: '<div><em/></div>',
            position: 'top left',
            offset: [1, 40]
        })
    
    /* Check the postcode */
    $('#quickButton').click(function() {

        $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'postcode=' + $('#postcode').val(),
                 success: function(data, textStatus) {
                    $("#selectOutput").html(data).slideDown("slow");
                 },
                 beforeSend: function(XMLHttpRequest) {
                    $('#fetch').fadeIn('medium');
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    $('#fetch').fadeOut('medium')
                    setTimeout("$('#quickButton').fadeIn('medium')", 500);
                    $('#postSelect').change(function() {
                        var selected_split = $(this).val().split(",");
                        $("#address1").val(selected_split[0]);
                        $("#address2").val(selected_split[1]);
                        $("#address3").val(selected_split[2]);
                        $("#selectOutput").slideUp("slow");
                    });
                },
                url: '{$_subdomain}/IFrame/PostcodeLookup'

        });//end ajax

        return false;

    });//end click
    
{* This section not for Swann Communications *}
{if $_skin ne ''}
    {* Update Service Types when Job Type changes *}
    $('#jobtype').change(function() {
        document.body.style.cursor = 'wait';
        $.ajax({ type: 'POST',
                 dataType: 'json',
                 data: 'JobTypeID=' + $('#jobtype').val(),
                 success: function(data, textStatus) {
                    var options, index, select, option;

                    // Get the raw DOM object for the select box
                    select = document.getElementById('service');

                    // Clear the old options
                    select.options.length = 0;

                    // Load the new options
                    options = data.options;
                    select.options.add(new Option('Please Select', ''));
                    for (index = 0; index < options.length; ++index) {
                        option = options[index];
                        select.options.add(new Option(option.text, option.value));
                    }
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    document.body.style.cursor = 'default';
                 },
                 url: '{$_subdomain}/IFrame/ServiceTypeLookup'

        });

        return false;
    
    });
    {* Clear Models when Manufacturer or UnitType changes *}
    $('#manufacturer, #product').change(function() {
        $("#model").val('');
        return false;
    });
    {* Update Models when Manufacturer or UnitType changes *}
    {* $('#manufacturer, #product').change(function() {
        document.body.style.cursor = 'wait';
        $.ajax({ type: 'POST',
                 dataType: 'json',
                 data: 'UnitTypeID=' + $('#product').val()+'&ManufacturerID='+$('#manufacturer').val(),
                 success: function(data, textStatus) {
                    var options, index, select, option;

                    // Get the raw DOM object for the select box
                    select = document.getElementById('model');

                    // Clear the old options
                    select.options.length = 0;

                    // Load the new options
                    options = data.options;
                    select.options.add(new Option('Please Select', ''));
                    for (index = 0; index < options.length; ++index) {
                        option = options[index];
                        select.options.add(new Option(option.text, option.value));
                    }
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    document.body.style.cursor = 'default';
                 },
                 url: '{$_subdomain}/IFrame/ModelLookup'

        });

        return false;
    
    }); *}
    {* Change Model input element when radio buttons selected *}
    $(document).on('click', "[name='LocateBy']", function() {
            console.log('LocateBy clicked Value: '+$(this).val());
            if($(this).val()=='1') {
                $("#ModelItem").show(); 
                $("#model").focus();
                $("#ProductDescriptionItem").hide(); 
                $("#product_description").val('');
                $("#product_description").removeAttr('required');
                $("#model").attr('required','required');
            } else {
                $("#ProductDescriptionItem").show();  
                $("#product_description").focus();
                $("#model").val(''); 
                $("#ModelItem").hide(); 
                $("#model").removeAttr('required');
                $("#product_description").attr('required','required');                
            }
        }
    ); 
    {* autocomplete for Model Number *}
    var autocomplete_cache = {};
    $("#model").autocomplete({
        source: 
            function(request, response) {
                var manufacturer_id = $("#manufacturer").val();
                var cache_key = manufacturer_id+request.term;
                if (cache_key in autocomplete_cache) {
                    response(autocomplete_cache[cache_key]);
                    return;
                }
                $.ajax({
                    url: "{$_subdomain}/Data/getModelNumbers/list=true/manufacturer="+manufacturer_id+"/",
                    dataType: "json",
                    data: { name_startsWith: request.term },
                    success: function(data) {
                        var response_data = $.map( data.models, function( item ) { return item.ModelNumber; });
                        autocomplete_cache[cache_key] = response_data;
                        response(response_data);
                    }
                });
            },
        minLength: 2,
        select: 
            function(event, ui) { 
                $.ajax({
                    url: "{$_subdomain}/Data/getModelNumbers/manufacturer=/",
                    dataType: "json",
                    data: { ModelNumber: ui.item.value },
                    success: function(data) {
                        $("#manufacturer").val(data.ManufacturerID);
                        $("#product").val(data.UnitTypeID);
                        $('#ModelID').val(data.ModelID);
                    }
                });
            },
        {* this code checks if autocomplete value has ben changed to plain text
           and resets the manufacturer and unit type to 'please select'
               also resets ModelID form element *}        
        change: 
            function(event, ui) {
                if (ui.item === null) {
                    {* code temporarily removed because not sure if this is the desired behaviour. *}
                    //$("#manufacturer").val('');
                    //$("#product").val('');
                    {* just clear the model & model id *}
                    $("#model").val('');
                    $("#ModelID").val('');
                } 
            } 
    }); 
{/if}
});
</script>

{/block}

 {block name=body}
        <div id="bookingFormHolder">


            <form id="bookingform" name="bookingform" action="{$_subdomain}/IFrame/JobBookingForm" method="post">
                
                <input type="hidden" name="NetworkID" id="NetworkID" value="{$NetworkID}" />
                <input type="hidden" name="NetworkName" id="NetworkID" value="{$NetworkName}" />
                <input type="hidden" name="ClientID" id="ClientID" value="{$ClientID}" />
                <input type="hidden" name="ClientName" id="ClientID" value="{$ClientName}" />
                <input type="hidden" name="BranchID" id="BranchID" value="{$BranchID}" />
                <input type="hidden" name="BrandID" id="BrandID" value="{$BrandID}" />
                <input type="hidden" name="ServiceProviderID" id="ServiceProviderID" value="{$ServiceProviderID}" />
                <input type="hidden" name="Username" id="Username" value="{$Username}" />
                <input type="hidden" name="CountryID" id="country" value="{$CountryID}" />
                <input type="hidden" name="ProductLocation" id="ProductLocation" value="{$ProductLocation}" />
                <input type="hidden" name="JobSourceID" id="JobSourceID" value="11" />
                
                <!-- personal details ======================================= -->
                <fieldset class="iBookingSet">

                    <!-- title -->
                    <div class="sectitle">Personal Details</div>

                    <div class="iBookingSetLeft">

                        <!-- select title -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="title">Title*:</label>
                            <select name="CustomerTitleID" id="title" required="required">
                                <option value="">Please select</option>
                                {foreach $titles as $row}
                                 <option value="{$row.CustomerTitleID}">{$row.Title|upper|escape:'html'}</option>
                                {/foreach}
                            </select>
                        </div>

                        <!-- first name -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="first">First Name*:</label>
                            <input type="text" name="ContactFirstName" id="first" maxlength="40" required="required" />
                        </div>

                        <!-- last name -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="last">Last Name*:</label>
                            <input type="text" name="ContactLastName" id="last" maxlength="40" required="required" />
                        </div>


                    </div> <!--  close set right -->

                    <div class="clear"></div>

                </fieldset>

                <!-- personal details ======================================= -->
                <fieldset class="iBookingSet">

                    <!-- title -->
                    <div class="sectitle">Contact Details</div>

                    <div class="iBookingSetLeft">

                        <!-- email address -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="email">Email Address*:</label>
                            <input type="email" name="ContactEmail" id="email" maxlength="40" required="required" />
                        </div>

                        <!-- phone number -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="phone">Phone Number**:</label>
                            <input type="text" name="ContactHomePhone" id="phone" data-group="1" maxlength="40" />
                        </div>

                        <!-- mobile number -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="mob">Mobile Number**:</label>
                            <input type="text" name="ContactMobile" id="mob" data-group="1" maxlength="40" />
                        </div>

                        <!-- postcode -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="postcode">Postcode*:</label>
                            <input class="short" type="text" name="PostalCode" id="postcode" title="SN15 3HJ" maxlength="10" required="required" /> <a href="javascript*:;">
                                <span id="quickButton" >Quick Find</span></a>
                                <span id="fetch" style="display: none">(fetching)</span>
                            <div id="selectOutput"></div>
                        </div>

                        <!-- House number -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="house">House Number/Name*:</label>
                            <input type="text" name="BuildingNameNumber" id="address1" maxlength="40" required="required" />
                        </div>

                        <!-- street -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="street">Street*:</label>
                            <input type="text" name="Street" id="address2" maxlength="40" required="required" />
                        </div>

                        <!-- town -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="town">Town/City*:</label>
                            <input type="text" name="City" id="address3" maxlength="60" required="required" />
                        </div>

                        <!-- County -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="county">County*:</label>
                            <select name="CountyID" id="county" required="required">
                                    <option value="">Please select</option>
                                    {foreach $counties as $row}
                                    <option value="{$row.CountyID}">{$row.Name|upper|escape:'html'}</option>
                                    {/foreach}
                            </select>
                        </div>


                    </div> <!--  close set right -->

                    <div class="clear"></div>

                </fieldset>

                <!-- personal details ======================================= -->
                <fieldset class="iBookingSet">

                    <!-- title -->
                    <div class="sectitle">Appliance Details</div>
                    <div class="iBookingSetLeft">
                        
                        <!-- job type -->                        
                       {* this section not for SWANN Communications *}
                       {if $_skin eq ''} 
                        <input type="hidden" name="JobTypeID" id="JobTypeID" value="{$job_types[0].JobTypeID}" />   
                       {else}
                        <div class="iBookingItem">
                            <label class="tLeft"  for="jobtype">Job Type*:</label>
                            <select name="JobTypeID" id="jobtype" required="required">
                                {foreach $job_types as $row}
                                <option value="{$row.JobTypeID}">{$row.Name|upper|escape:'html'}</option>
                                {foreachelse}
                                <option value="">Please select</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}

                        <!-- service required -->
                       {* this section not for SWANN Communications *}
                       {if $_skin eq ''} 
                        <input type="hidden" name="ServiceTypeID" id="ServiceTypeID" value="{$service_types[0].ServiceTypeID}" />   
                       {else}
                        <div class="iBookingItem">
                            <label class="tLeft"  for="service">Service Required*:</label>
                            <select name="ServiceTypeID" id="service" required="required">
                                {foreach $service_types as $row}
                                {if $row@first and $row@total gt 1}
                                <option value="">Please select</option>
                                {/if}
                                <option value="{$row.ServiceTypeID}">{$row.Name|upper|escape:'html'}</option>
                                {foreachelse}
                                <option value="">&nbsp;</option>
                                {/foreach}
                            </select>
                        </div>
                        {/if}
                        
                       {* this section not for SWANN Communications *}
                       {if $_skin eq ''}
                       <input type="hidden" name="ModelID" id="ModelID" value="{$ModelID}" />
                       {else}
                        <!-- Model -->
                        <div class="iBookingItem">
                            <label class="tLeft">&nbsp;</label>
                            <input  type="radio" name="LocateBy"  value="1" checked="checked"  /><span>Search by model number</span> 
                            <input  type="radio" name="LocateBy"  value="2" /><span>Enter free text description</span> 
                        </div>
                        <div id="ModelItem" class="iBookingItem">
                            <label class="tLeft"  for="model">Model*:</label>
                            <input type="text" name="ModelName" id="model" maxlength="30" required="required" />
                            <input type="hidden" name="ModelID" id="ModelID" value="" />
                            {*<select name="ModelID" id="model" required="required">
                                {foreach $models as $row}
                                {if $row@first and $row@total gt 1}
                                <option value="">Please select</option>
                                {/if}
                                <option value="{$row.ModelID}">{$row.ModelNumber|escape:'html'}</option>
                                {foreachelse}
                                <option value="">Please select;</option>                                    
                                {/foreach}
                            </select>*}
                        </div>
                        <div id="ProductDescriptionItem" class="iBookingItem" style="display: none">
                            <label class="tLeft"  for="product_description">Product Description*:</label>
                            <input type="text" name="ProductDescription" id="product_description" maxlength="24" />
                        </div>
                        {/if}
                        
                        <!-- Manufacturer -->
                       {* this section not for SWANN Communications *}
                       {if $_skin eq ''} 
                        <input type="hidden" name="ManufacturerID" id="ManufacturerID" value="{$manufacturers[0].ManufacturerID}" />   
                       {else}                        
                        <div class="iBookingItem">
                            <label class="tLeft"  for="manufacturer">Manufacturer*:</label>
                            <select name="ManufacturerID" id="manufacturer" required="required">
                                {foreach $manufacturers as $row}
                                {if $row@first and $row@total gt 1}
                                <option value="">Please select</option>
                                {/if}                                        
                                <option value="{$row.ManufacturerID}">{$row.ManufacturerName|upper|escape:'html'}</option>
                                {foreachelse}
                                <option value="">Please select</option>                                    
                                {/foreach}
                            </select>
                        </div>
                        {/if}

                        <!-- Product Type -->
                        <div class="iBookingItem">
                            <label class="tLeft"  for="product">Product Type*:</label>
                            <select name="UnitTypeID" id="product" required="required">
                                {foreach $product_types as $row}
                                {if $row@first and $row@total gt 1}
                                <option value="">Please select</option>
                                {/if}                                        
                                <option value="{$row.UnitTypeID}">{$row.UnitTypeName|upper|escape:'html'}</option>
                                {foreachelse}
                                <option value="">Please select</option> 
                                {/foreach}
                            </select>
                        </div>
                                    
                        <!-- Notes -->
                        <div class="iBookingItem">
                            {* This section for Swann Communications *}
                            {if $_skin eq ''}
                            <label class="tLeft"  for="ReportedFault">Notes:</label>
                            {else}
                            <label class="tLeft"  for="ReportedFault">Notes/Fault Description:</label>
                            {/if}
                            <textarea name="ReportedFault"></textarea>
                        </div>

                    </div> <!--  close set right -->

                    <div class="clear"></div>

                </fieldset>

                <!-- submit form ======================================= -->
                <fieldset class="iBookingSet">

                    <!-- street -->
                    <div class="iBookingItem center">
                        {* This section for Swann Communications *}
                        {if $_skin eq ''}
                        <input type="submit" name="submit" class="form-button" value="Obtain Quote/Book Installation" />
                        {else}
                            <input type="submit" name="submit" class="form-button" value="Book Service" />
                        {/if}
                        <p class="small">Please fill out all fields marked * and at least one field marked **</p>
                    </div>

                </fieldset>

            </form>

        </div> <!--  close booking holder -->

{/block}