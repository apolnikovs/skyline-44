{* extends "./TestLayout.tpl" *}

 {block name=body}
 <style>
     #BookCollectionForm { width: 600px; }
     #BookCollectionForm label { width: 200px; }
     #BookCollectionForm label sup { font-weight:bold; position: absolute; right: -0.6em; top: 0.6em; }
     #BookCollectionForm label.fieldError { margin-left: 215px; clear: left;}
     #bcfEditButton { text-decoration: underline;
                   font-size: 1.2em;
                   font-weight: bold; }
     #bcfCollectionDate { width: 273px; margin-right: 3px; }
     #bcfPostcode { width: 162px; }
     #BookCollectionForm .form_buttons_container { width: 527px; display: inline-block; text-align: right; }
     #bcf_quick_find_btn { background-position: 0px 0px; }
     #bcf_quick_find_btn:hover { background-position: 0px -21px; }
</style>

<script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript">

{**********************************************************************
   IMPORTANT: html identifier names on this dialog must be prefixed with 
   bcf in order to avoid conflicts with any parent page identifier names.
   
   If displaying this dialog form from the context of a job booking page
   before the job has been booked (i.e. there is not yet a valid JobID)
   create a copy of this object constructor and populate the object
   properties with values from the job booking form.
   
   It will then be used by this form's javascript to initialise
   theBook Collection form fields.
   
   Upon submit Book Collection form data will be saved in the Session Data
   enabling the job booking process to book the Courier collection once the
   booking is confirmed and committed to the database.
   
   ********************************************************************}
{*  
function BookCollectionFormData() {

    this.CollectionDate = '';
    this.PreferredCourier = '';    
    this.ConsignmentNo = '';
    this.Name = '';
    this.Postcode = '';
    this.BuildingName = '';
    this.Street = '';
    this.Area = '';
    this.Town = '';
    this.County = '';
    this.Country = '';
    this.Email = '';
    this.Phone = ''; 
    this.PreferredCourierID = null;
    this.CountyID = null;
    this.CountryID = null;
    
    this.update = function() {
        // this function is called when Courier Details are updated
        // by the dialog form submit.
        // synchronise your parent page data here
        console.log('BookCollectionFormData update()');
    };
}
*}
    
function do_resize() {
    $.colorbox.resize();
}
    
$(document).ready(function() {

    var job_data;
    if (typeof BookCollectionFormData === 'function') {
        job_data = new BookCollectionFormData();
    } else {
        job_data = new function() {
        
                        this.CollectionDate = '';
                        this.PreferredCourier = '';
                        this.ConsignmentNo = '';
                        this.PackagingType = '';
                        this.Name = '';
                        this.Postcode = '';
                        this.BuildingName = '';
                        this.Street = '';
                        this.Area = '';
                        this.Town = '';
                        this.County = '';
                        this.Country = '';
                        this.Email = '';
                        this.Phone = ''; 
                        this.PreferredCourierID = null;
                        this.CountyID = null;
                        this.CountryID = null;
                        
                        this.update = function() {};
                  };
    }
    
    {if (!isset($job_id))}   
    $( '#bcfCollectionDate' ).val(job_data.CollectionDate);
    $( '#bcfPreferredCourier' ).val(job_data.PreferredCourierID);
    setValue("#bcfPreferredCourier");
    $( '#bcfConsignmentNo' ).val(job_data.ConsignmentNo);
    $( '#bcfPackagingType' ).val(job_data.PackagingType);
    setValue("#bcfPackagingType");
    $( '#bcfName' ).val(job_data.Name);
    $( '#bcfPostcode' ).val(job_data.Postcode);
    $( '#bcfBuildingName' ).val(job_data.BuildingName);
    $( '#bcfStreet' ).val(job_data.Street);
    $( '#bcfArea' ).val(job_data.Area);
    $( '#bcfTown' ).val(job_data.Town);
    $( '#bcfSelectCounty .countySelect' ).val(job_data.CountyID);
    setValue("#bcfSelectCounty .countySelect");
    $( '#Country' ).val(job_data.CountryID);
    setValue("#bcfCountry");
    $( '#bcfEmail' ).val(job_data.Email);  
    $( '#bcfPhone' ).val(job_data.Phone);
    {/if}
        
    {* Configure JQuery UI datepicker for Collection Date *}
    $( '#bcfCollectionDate' ).datepicker({
            dateFormat: 'dd/mm/yy',
            showOn: 'button',
            buttonImage: '{$_subdomain}/css/Skins/{$_theme}/images/calendar.png',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            minDate: 0
      });
      
    {* Change Form labels when inbound/outbound radio button changes *}
    $("input[name='direction']").change(function() {
        if ($('input:radio[name=direction]:checked').val() == 'inbound') {
            $('#bcfCollectionLegend').html("{$page['Labels']['collection_legend']|escape:'html'}");
            $('#bcfCollectionDateLabel').html("{$page['Labels']['collection_date']|escape:'html'}");
            $('#bcfEditButton').html("{$page['Buttons']['edit_collection_address']|escape:'html'}");
        } else {
            $('#bcfCollectionLegend').html("{$page['Labels']['delivery_legend']|escape:'html'}");
            $('#bcfCollectionDateLabel').html("{$page['Labels']['delivery_date']|escape:'html'}");
            $('#bcfEditButton').html("{$page['Buttons']['edit_delivery_address']|escape:'html'}");
        }
      });
      
    {* Expand/Collapse the Collection Address Panel *}
    $('#bcfEditButton').click(function() {
            $('#bcfCollectionAddressPanel').toggle();
            $.colorbox.resize();      
      });
      
    {* Close the colorbox dialog *}
    $('#bcfCancelButton').click(function() {
            $.colorbox.close();    
      })
      
    {* Update counties when county changes *}
    $('#bcfCountry').change(function() {
            document.body.style.cursor = 'wait';
            $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'CountryID=' + $(this).val(),
                 success: function(data, textStatus) {
                    $('#bcfSelectCounty').html(data);
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    document.body.style.cursor = 'default';
                 },
                 url: '{$_subdomain}/Courier/CountyLookup'

            });

            return false;
    
    });
    
    {* Update Packaging Types  when Courier changes *}
    $('#bcfPreferredCourier').change(function() {
            document.body.style.cursor = 'wait';
            $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'CourierID=' + $(this).val(),
                 success: function(data, textStatus) {
                    $('#bcfPackagingType').html(data);
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    document.body.style.cursor = 'default';
                 },
                 url: '{$_subdomain}/Courier/PackagingTypesLookup'

            });

            return false;
    
    });
      
    {* use AJAX to lookup the collection address from the postcode *}
    $('#bcf_quick_find_btn').click(function() {

        $.ajax({ type: 'POST',
                 dataType: 'html',
                 data: 'Postcode=' + $('#bcfPostcode').val(),
                 success: function(data, textStatus) {
                    $("#bcfSelectOutput").html(data).slideDown("slow");
                 },
                 beforeSend: function(XMLHttpRequest) {
                    $('#bcf_fetch').fadeIn('medium');
                 },
                 complete: function(XMLHttpRequest, textStatus) {
                    $('#bcf_fetch').fadeOut('medium');
                    $('#bcfSelectOutput .postSelect').change(function() {

                        var selected_split = $(this).val().split(",");
                                               
                        $("#bcfBuildingName").val(trim(selected_split[0]+' '+selected_split[6])).blur();	
                        $("#bcfStreet").val(trim(selected_split[1])).blur();
                        $("#bcfArea").val(trim(selected_split[2])).blur();
                        $("#bcfTown").val(trim(selected_split[3])).blur();
                
                        $("#bcfSelectCounty .countySelect").val(trim(selected_split[4])).blur();
                        setValue("#bcfSelectCounty .countySelect");
                
                        $("#bcfCountry").val(trim(selected_split[5])).blur();
                        setValue("#bcfCountry");
                
                        $("#bcfSelectOutput").slideUp("slow");
                    });
                 },
                 url: '{$_subdomain}/Courier/PostcodeLookup'

            });

            return false;

        });
        
    {* Validate and Submit the Book Courier Collection Form *}
    $("#BookCollectionForm").validate({
            onkeyup: false,
            onblur: false,
            ignore: '',
            errorElement: 'label',
            errorClass: 'fieldError',
            {if isset($job_id)}
                {if $from_job_booking_page}
                rules: {
                    CollectionDate: 'required',
                    PreferredCourier: 'required',
                    Postcode: 'required',
                    Street: 'required',
                    Country: 'required'
                },
                {else}
                rules: {
                    Postcode: 'required',
                    Street: 'required',
                    Country: 'required'
                },
                {/if}
            {else}
            rules: {
                CollectionDate: 'required'
                PreferredCourier: 'required'
            },
            {/if}
            errorPlacement: function(error, element) {
                error.appendTo( element.parent("p"));
            },
            submitHandler: function(form) {
                                $("button, input[type='button']").attr("disabled", "true");
                                $('*').css('cursor','wait');
                                $(form).ajaxSubmit(function() {
                                              $('*').css('cursor','default');
                                              // pass data back to parent page...
                                              job_data.CollectionDate = $('#bcfCollectionDate').val();
                                              job_data.PreferredCourier = $('#bcfPreferredCourier').val() == '' ? '' : $('#bcfPreferredCourier option:selected').text();
                                              job_data.ConsignmentNo = $('#bcfConsignmentNo').val();
                                              job_data.PackagingType = $('#bcfPackagingType option:selected').val();
                                              job_data.Name = $('#bcfName').val();
                                              job_data.Postcode = $('#bcfPostcode').val();
                                              job_data.BuildingName = $('#bcfBuildingName').val();
                                              job_data.Street = $('#bcfStreet').val();
                                              job_data.Area = $('#bcfArea').val();
                                              job_data.Town = $('#bcfTown').val();
                                              job_data.County = $('#bcfSelectCounty .countySelect option:selected').text();
                                              job_data.Country = $('#bcfCountry option:selected').text();
                                              job_data.Email = $('#bcfEmail').val();
                                              job_data.Phone = $('#bcfPhone').val();
                                              job_data.PreferredCourierID = $('#bcfPreferredCourier').val();
                                              job_data.CountyID = $('#bcfSelectCounty .countySelect').val();
                                              job_data.CountryID = $('#bcfCountry').val();
                                              job_data.update();
                                              $.colorbox.close(); 
                                        });
                                return false;
                            },
            invalidHandler: function(event, validator) {
                {if isset($job_id)}
                {* if any errors make sure the hidden address form panel is visible *}
                $('#bcfCollectionAddressPanel').show();
                {* delay resize 100ms to give validate time to render error messages... *}
                var timeoutID = window.setTimeout(do_resize, 100);
                {/if}
            }
        });
             
});
</script>
     
<form id="BookCollectionForm" name="BookCollectionForm" action="{$_subdomain}/Courier/BookCollectionForm" class="inline" method="post" >
    <fieldset>
        <legend id="bcfCollectionLegend" title="">{$page['Labels']['collection_legend']|escape:'html'}</legend>
        <input type="hidden" name="JobID" value="{$job_id}" />
        <p>
            <label >&nbsp;</label>
            <input type="radio" name="Direction" value="inbound" checked >{$page['Labels']['inbound']|escape:'html'}</input>
            <input type="radio" name="Direction" value="outbound">{$page['Labels']['outbound']|escape:'html'}</input>
        </p>
        <p>
            <label id="bcfCollectionDateLabel" for="bcfCollectionDate">{$page['Labels']['collection_date']|escape:'html'}:{if $from_job_booking_page}<sup>*</sup>{/if}</label>
            <input type="text" id="bcfCollectionDate" name="CollectionDate" class="text" 
                   value="{$collection['collection_date']|default: ''|date_format: '%d/%m/%y'|escape:'html'}" />
        </p>
        <p>
            <label for="PreferredCourier">{$page['Labels']['preferred_courier']|escape:'html'}:{if $from_job_booking_page}<sup>*</sup>{/if}</label>
            <select name="PreferredCourier" id="bcfPreferredCourier" class="text" >
                <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
		{foreach $couriers as $courier}
                <option value="{$courier.CourierID}" {if $courier.CourierID eq $collection['courier_id']|default: ''}selected{/if}>{$courier.CourierName|upper|escape:'html'}</option>
		{/foreach}
            </select>
        </p>
        <p>
            <label for="bcfConsignmentNo">{$page['Labels']['consignment_no']|escape:'html'}:</label>
            <input type="text" id="bcfConsignmentNo" name="ConsignmentNo" class="text" 
                   value="{$collection['consignment_no']|default: ''|escape:'html'}" />
        </p>
        
        <p>
            <label for="PackagingType">{$page['Labels']['packaging_type']|escape:'html'}:</label>
            <span id="bcfPackagingTypeContainer">
            <select name="PackagingType" id="bcfPackagingType" class="text" >
                <option value="" selected="selected">{$page['Text']['please_select']|escape:'html'}</option>
		{foreach $packaging_types as $packaging_type}
                <option value="{$packaging_type}" {if $packaging_type eq $collection['packaging_type']|default: ''}selected{/if}>{$packaging_type|upper|escape:'html'}</option>
		{/foreach}
            </select>
            </span>
        </p>
        {if isset($job_id)}
        <p>
            <br />
            <label><a id="bcfEditButton" href="#">{$page['Buttons']['edit_collection_address']|escape:'html'}</a></label>
            <br />&nbsp;<br />
        </p>
        {/if}
        <div id="bcfCollectionAddressPanel" style="display: none;">
            <p>
                <label for="bcfName">{$page['Labels']['name']|escape:'html'}:</label>
                <input type="text" id="bcfName" name="Name" class="text" 
                       value="{$collection['name']|default: ''|escape:'html'}" />
            </p>  
            <p>
                <label for="bcfPostcode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                <input type="text" id="bcfPostcode" name="Postcode" class="text" 
                       value="{$collection['address']['postcode']|default: ''|escape:'html'}" />
                <input type="button" name="quick_find_btn" id="bcf_quick_find_btn" style="" class="textSubmitButton postCodeLookUpBtn"  value="{$page['Buttons']['find_address']|escape:'html'}" />
                <img id="bcf_fetch" src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" style="position: relative; top: 4px; display: none;" />
            </p>
            <p id="bcfSelectOutput" style="display: none;"></p>
            <p>
                <label for="bcfBuildingName">{$page['Labels']['building_name']|escape:'html'}:</label>
                <input type="text" id="bcfBuildingName" name="BuildingName" class="text" 
                       value="{$collection['address']['building']|default: ''|escape:'html'}" />
            </p>
            <p>
                <label for="bcfStreet">{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                <input type="text" id="bcfStreet" name="Street" class="text" 
                       value="{$collection['address']['street']|default: ''|escape:'html'}" />
            </p>
            <p>
                <label for="bcfArea">{$page['Labels']['area']|escape:'html'}:</label>
                <input type="text" id="bcfArea" name="Area" class="text" 
                       value="{$collection['address']['area']|default: ''|escape:'html'}" />
            </p>
            <p>
                <label for="bcfTown">{$page['Labels']['town']|escape:'html'}:</label>
                <input type="text" id="bcfTown" name="Town" class="text" 
                       value="{$collection['address']['town']|default: ''|escape:'html'}" />
            </p>
            <p>
                <label for="County">{$page['Labels']['county']|escape:'html'}:</label>
                <span id="bcfSelectCounty">
                <select name="County" class="countySelect text" >
                    <option value="" selected>{$page['Text']['please_select']|escape:'html'}</option>
                    {foreach $counties as $county}
                    <option value="{$county.CountyID}" {if $county.CountyID eq $collection['county_id']|default: ''}selected{/if}>{$county.Name|upper|escape:'html'}</option>
                    {/foreach}
                </select>
                </span>
            </p>
            <p>
                <label for="bcfCountry">{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                <select name="Country" id="bcfCountry" class="text" >
                    {* <option value="" selected>{$page['Text']['please_select']|escape:'html'}</option> *}
                    {foreach $countries as $country}
                    <option value="{$country.CountryID}" {if $country.Name eq 'UK'}selected{/if}>{$country.Name|upper|escape:'html'}</option>
                    {/foreach}
                </select>
            </p>
            <p>
                <label for="bcfEmail">{$page['Labels']['email']|escape:'html'}:</label>
                <input type="text" id="bcfEmail" name="Email" class="text" 
                       value="{$collection['email']|default: ''|escape:'html'}" />
            </p>
            <p>
                <label for="bcfPhone">{$page['Labels']['phone']|escape:'html'}:</label>
                <input type="text" id="bcfPhone" name="Phone" class="text" 
                       value="{$collection['phone']|default: ''|escape:'html'}" />
            </p>
        </div>
        <p class="form_buttons_container">
            <button type="submit" class="gplus-blue"><span class="label">{$page['Buttons']['save']|escape:'html'}</span></button>
            <button id="bcfCancelButton" type="button" class="gplus-blue"><span class="label">{$page['Buttons']['cancel']|escape:'html'}</span></button>
        </p>
    </fieldset>
</form>

 {/block}